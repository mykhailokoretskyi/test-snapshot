package Test::Snapshot;

use 5.008;
use strict;
use warnings;
use Data::Dumper;

=head1 NAME

Test::Snapshot - makes unit testing fast and easy by testing agains previous
stored test result.

=head1 VERSION

Version 0.0.1

=cut

our $VERSION = '0.0.1';

$Data::Dumper::Sortkeys = 1;

our @ISA = qw( Exporter );
our @EXPORT = qw( done_testing );

use constant READ => '<';
use constant WRITE => '>';
use constant SNAPSHOT_EXT => '.snap';
use constant SNAPSHOT_SEPARATOR => "======== SNAPSHOT SEPARATOR ========\n";

DESTROY {
    my $self = shift;

    if (!$self->{has_snapshot} || $self->{update_snapshot}) {
        $self->_write_snapshot();
    }
}

sub import {
    my ($class, %args) = @_;

    require Test::More;
    Test::More->import(
        $args{tests} ? ( tests => $args{tests} ) : ()
    );
    $class->export_to_level(1, "done_testing");
}

sub new {
    my ($class, %args) = @_;

    my $update_snapshot = _is_requested_override();

    my $self = {
        file_name => $0,
        tests => [],
        snapshots => [],
        $update_snapshot ? (update_snapshot => 1) : (),
    };

    bless $self, $class;

    $self->_load_snapshot();

    return $self;
}

sub test {
    my ($self, %args) = @_;

    my $test = Test::Snapshot::Snap->new(
        name => $args{name},
        has_snapshot => 1,
        snap => Dumper $args{instance},
    );

    # Name of the tests should not be duplicated
    if ($self->_get_test_by_name($args{name})) {
        diag("Duplicated name of the snapshot test: `$args{name}`");
        fail($args{name});
        $test->result(0);
        push @{$self->{tests}}, $test;
        return $test->result();
    }

    my $snapshot = $self->_get_snapshot_by_name($args{name});
    if (!$snapshot || $self->{update_snapshot}) {
        pass($args{name});
        $test->result(1);
        $test->has_snapshot(0);
        push @{$self->{tests}}, $test;
        return $test->result();
    }

    $test->result( is($test->{snap}, $snapshot->{snap}, $test->{name}) );
    push @{$self->{tests}}, $test;

    $test->result();
}

sub _load_snapshot {
    my ($self, %args) = @_;
    my $FILE = $self->_get_file_handle(
        mode => READ()
    );

    unless ($FILE) {
        $self->{has_snapshot} = 0;
        return;
    }

    $self->{has_snapshot} = 1;

    {
        local $/;
        my $content = <$FILE>;
        close $FILE;
        $self->_parse_snapshot($content);
    }
}

sub _write_snapshot {
    my $self = shift;

    my $FILE = $self->_get_file_handle(mode => WRITE());
    my @snaps;

    foreach my $test (@{$self->{tests}}) {
        my $snap = "[% " . $test->{name} . " - snapshot %]\n" . $test->{snap} . "\n";
        push @snaps, $snap;
    }

    print $FILE join(SNAPSHOT_SEPARATOR(), @snaps);
    close $FILE;
}

sub _get_file_handle {
    my ($self, %args) = @_;
    my $mode = $args{mode};
    my $file_name = $self->{file_name} . SNAPSHOT_EXT();

    if (-e $file_name || $mode eq WRITE()) {
        open(my $FH, $mode, $file_name ) || return _cannot_open_file($file_name);
        return $FH;
    }

    return;
}

sub _cannot_open_file {
    my $file_name = shift;
    print STDERR "Cannot open snapshot file `$file_name`";
    return;
}

sub _parse_snapshot {
    my ($self, $snapshot) = @_;

    my @snaps = split SNAPSHOT_SEPARATOR(), $snapshot;

    foreach my $snap (@snaps){
        $snap =~ /(^\[% (.*) - snapshot %\]\n((.*\n?)+)\n)$/;
        push @{$self->{snapshots}}, Test::Snapshot::Snap->new(
            name => $2,
            snap => $3
        );
    }
}

sub _get_snapshot_by_name {
    my ($self, $name) = @_;
    _get_by_name( $self->{snapshots}, $name );
}

sub _get_test_by_name {
    my ($self, $name) = @_;
    _get_by_name( $self->{tests}, $name );
}

sub _get_by_name {
    my ($where, $what) = @_;
    my @items = grep { $_->{name} eq $what } @$where;

    if (scalar @items > 1){
        print STDERR "Found multiple snapshots/tests with the same name ($what)\n";
    }

    return $items[0];
}

sub done_testing {
    Test::More::done_testing;
}

sub _is_requested_override {
    return grep {$_ eq '-u'} @ARGV;
}

1;

package Test::Snapshot::Snap;

sub new {
    my ($class, %args) = @_;
    chomp $args{snap};
    return bless \%args, $class;
}

sub result {
    my ($self, $value) = @_;
    $self->{result} = $value if defined $value;
    $self->{result};
}

sub has_snapshot {
    my ($self, $value) = @_;
    $self->{has_snapshot} = $value if defined $value;
    $self->{has_snapshot};
}

1;




=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Test::Snapshot;

    my $foo = Test::Snapshot->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 function1

=cut

sub function1 {
}

=head2 function2

=cut

sub function2 {
}

=head1 AUTHOR

Mykhailo Koretskyi, C<< <radio.mkor at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-test-snapshot at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Test-Snapshot>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Test::Snapshot


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Test-Snapshot>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Test-Snapshot>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Test-Snapshot>

=item * Search CPAN

L<http://search.cpan.org/dist/Test-Snapshot/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2017 Mykhailo Koretskyi.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of Test::Snapshot
