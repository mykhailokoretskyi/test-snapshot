use 5.008;
use strict;
use warnings;
use Test::More;
use Data::Dumper;

use Test::Snapshot;

my $snap = Test::Snapshot->new();
is(ref $snap, 'Test::Snapshot', "Instance is created");

# _get_by_name
my $items = [
    {
        name => 'first'
    },
    {
        name => 'second'
    },
    {
        name => 'third'
    },
    {
        name => 'fourth'
    },
];

is(Test::Snapshot::_get_by_name($items, 'first')->{name}, 'first', 'returns single value by name');

$items->[1]->{name} = 'third';
is(Test::Snapshot::_get_by_name($items, 'third')->{name}, 'third', 'returns single value by name for duplicated');
$items->[1]->{name} = 'second';

# _get_test_by_name
$snap->{tests} = $items;
is($snap->_get_test_by_name('third')->{name}, "third", 'returns tests by name');

my $snapshots = \@{$items};
$snap->{snapshots} = $snapshots;
is($snap->_get_snapshot_by_name('third')->{name}, "third", 'returns snapshots by name');

# _cannot_open_file
is(Test::Snapshot::_cannot_open_file('myFile'), undef, "_cannot_open_file returns undef");

done_testing();

#clean up snap Instance
$snap->{tests} = [];
$snap->{snapshots} = [];
*Test::Snapshot::_get_file_handle = sub {};
