use 5.008;
use strict;
use warnings;
use Test::More;
use Data::Dumper;

use_ok( 'Test::Snapshot', 'tests', 2);
use_ok( 'Test::Snapshot' );

done_testing();
